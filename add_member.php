<?php
   session_start();
   require_once "connection.php";
    
    if (isset($_POST['submit'])) {
        $firstname = isset($_POST['Member_firstname']) && !empty($_POST['Member_firstname']) ? trim($_POST['Member_firstname']) : '';
        $lastname = isset($_POST['Member_lastname']) && !empty($_POST['Member_lastname']) ? trim($_POST['Member_lastname']) : '';
        $mail = isset($_POST['Member_mail']) && !empty($_POST['Member_mail']) ? filter_var($_POST['Member_mail'], FILTER_SANITIZE_EMAIL) : '';
        $status = isset($_POST['Member_status']) && !empty($_POST['Member_status']) ? trim($_POST['Member_status']) : '';
    
        
        if (empty($firstname) || empty($lastname) || empty($mail) || empty($status)) {
            $_SESSION['error'] = "กรุณากรอกข้อมูลในช่องที่จำเป็นทั้งหมด";
            header("location: member.php");  
            exit(); 
        }
    
        
        $sql = $conn->prepare("INSERT INTO member(Member_firstname, Member_lastname, Member_mail, Member_status) VALUES(:Member_firstname, :Member_lastname, :Member_mail, :Member_status)");
        $sql->bindParam(":Member_firstname", $firstname);
        $sql->bindParam(":Member_lastname", $lastname);
        $sql->bindParam(":Member_mail", $mail);
        $sql->bindParam(":Member_status", $status);
        
        try {
        $sql->execute(); 
        $last_id = $conn->lastInsertId(); // ดึงค่า Member_id ล่าสุด
        $_SESSION['success'] = "Data has been inserted successfully! Member ID: " . $last_id;
        header("location: member.php");
        exit(); 
        } catch (PDOException $e) {
        $_SESSION['error'] = "Data has not been inserted successfully" . $e->getMessage() . " (SQL Error Code: " . $e->getCode() . ")";
        header("location: member.php");
        exit(); 
        }
        }
        
        ?>