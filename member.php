<?php
    require_once('connection.php')   
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TAG Project</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php include('nav.php'); ?>
        <!-- End of Sidebar -->

        <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">       
    </head>
    <body>   
        
        <div class="modal fade" id="userModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="add_member.php" method="post" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="Member_firstname" class="col-form-label">First Name:</label>
                            <input type="text" required class="form-control" name="Member_firstname">
                        </div>
                        <div class="mb-3">
                            <label for="Member_lastname" class="col-form-label">Last Name:</label>
                            <input type="text" required class="form-control" name="Member_lastname">
                        </div>
                        <div class="mb-3">
                            <label for="Member_mail" class="col-form-label">mail:</label>
                            <input type="text" required class="form-control" name="Member_mail">
                        </div>
                        <div class="mb-3">
                            <label for="Member_password" class="col-form-label">password:</label>
                            <input type="text" required class="form-control" name="Member_password">
                        </div>
                        <div class="mb-3">
                            <label for="Member_status" class="col-form-label">status:</label>
                            <input type="text" required class="form-control" name="Member_status">
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" name="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
                
                </div>
            </div>
        </div>
    </body>

             <!--Tables -->
             <div class="col-xl-12 col-lg-10"> 
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Member</h6>
                    </div>
                        <div class="col-md-6 mt-3 d-flex">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#userModal" data-bs-whatever="@mdo">Add User</button>
                        </div>
                    <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                            <th>ID.</th>
                            <th>firstname</th>
                            <th>lastname</th>
                            <th>mail</th>
                            <th>password</th>
                            <th>status</th>
                            <th>Edit</th>
                            <th>delete</th>
                        </thead>
                        <tbody>

                        <?php
                            // เชื่อมต่อกับฐานข้อมูล (แทนที่ด้วยข้อมูลของคุณ)
                            $servername = "localhost";
                            $username = "root";
                            $password = "1234";
                            $dbname = "tagpoject";

                            $conn = new mysqli($servername, $username, $password, $dbname);

                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            }

                            // เลือกข้อมูลจากตาราง member
                            $sql = "SELECT * FROM member";
                            $result = $conn->query($sql);

                            if(isset($_REQUEST['delete_Member_id'])){
                                $id = $_REQUEST['delete_Member_id'];
                              
                                // เตรียม DELETE statement
                                $delete_stmt = $conn->prepare("DELETE FROM member WHERE Member_id = ?");
                              
                                // แปลง id เป็น integer
                                $id = intval($id);
                              
                                // Bind parameter
                                $delete_stmt->bind_param('i', $id);
                              
                                // เรียกใช้งาน prepared statement
                                $delete_stmt->execute();
                              
                                // Redirect หลังลบข้อมูลสำเร็จ (เลือกใช้ได้)
                                header('Location:member.php');
                              }

                            if ($result->num_rows > 0) {
                                // วนซ้ำแต่ละแถวและแสดงข้อมูลในตาราง
                                while ($row = $result->fetch_assoc()) {
                                    $member_id = $row["Member_id"]; // ดึง Member ID จากแถวปัจจุบัน

                                        echo "<tr>";
                                        echo "<td>" . $row["Member_id"] . "</td>";
                                        echo "<td>" . $row["Member_firstname"] . "</td>";
                                        echo "<td>" . $row["Member_lastname"] . "</td>";
                                        echo "<td>" . $row["Member_email"] . "</td>";
                                        echo "<td>" . $row["Member_password"] . "</td>";
                                        echo "<td>" . $row["Member_status"] . "</td>";
                                        echo "<td> <a href='Edit_Member.php?update_Member_id=". $member_id ."' class='btn btn-warning btn-icon-split'> <span class='icon text-white-50'> <i class='fas fa-fw fa-wrench'></i> </span>
                                         <span class='text'>Edit</span> </a></td>";
                                        echo "<td> 
                                        <a onclick='confirm('Are you sure you want to delete?');' href='?delete_Member_id=" . $member_id . "' class='btn btn-danger btn-icon-split'> 
                                            <span class='icon text-white-50'> 
                                                <i class='fas fa-trash'></i> 
                                            </span>
                                            <span class='text'>delete</span> 
                                        </a>
                                    </td>";
                                   echo "</tr>";
                               }
                           } else {
                               echo "ไม่พบข้อมูล";
                           }
                           
                           $conn->close();
                           ?>

                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

    <script>src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"</script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>



