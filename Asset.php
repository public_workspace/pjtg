<?php
    $servername = "localhost";
    $username = "root";
    $password = "1234";
    $dbname = "tagpoject";

    try {

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully!";

    } catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TAG Project</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php include('nav.php'); ?>
        <!-- End of Sidebar -->
                     <!--Tables -->
                     <div class="col-xl-12 col-lg-10"> 
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Asset</h6>
                    </div>
                        <div class="col-md-6 mt-3 d-flex">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#userModal" data-bs-whatever="@mdo">Add Asset</button>
                        </div>
                    <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                            <th>ID.</th>
                            <th>Mac address</th>
                            <th>Name Asset</th>
                            <th>type</th>
                            <th>Edit</th>
                            <th>delete</th>
                        </thead>
                        <tbody>

                        <?php
                            // เชื่อมต่อกับฐานข้อมูล (แทนที่ด้วยข้อมูลของคุณ)
                            $servername = "localhost";
                            $username = "root";
                            $password = "1234";
                            $dbname = "tagpoject";

                            $conn = new mysqli($servername, $username, $password, $dbname);

                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            }

                            $sql = "SELECT * FROM sensors";
                            $result = $conn->query($sql);

                            if(isset($_REQUEST['delete_s_no'])){
                                $id = $_REQUEST['delete_s_no'];
                              
                                // เตรียม DELETE statement
                                $delete_stmt = $conn->prepare("DELETE FROM sensors WHERE s_no = ?");
                              
                                // แปลง id เป็น integer
                                $id = intval($id);
                              
                                // Bind parameter
                                $delete_stmt->bind_param('i', $id);
                              
                                // เรียกใช้งาน prepared statement
                                $delete_stmt->execute();  
                              
                                // Redirect หลังลบข้อมูลสำเร็จ (เลือกใช้ได้)
                                header('Location:Asset.php');
                              }

                            if ($result->num_rows > 0) {
                                // วนซ้ำแต่ละแถวและแสดงข้อมูลในตาราง
                                while ($row = $result->fetch_assoc()) {
                                    $s_no = $row["s_no"]; // ดึง Member ID จากแถวปัจจุบัน

                                    if ($row["s_device_type"] === "arubaTag") {

                                        echo "<tr>";
                                        echo "<td>" . $row["s_no"] . "</td>";
                                        echo "<td>" . $row["s_mac_address"] . "</td>";
                                        echo "<td>" . $row["s_minor"] . "</td>";
                                        echo "<td>" . $row["s_device_type"] . "</td>";
                                        echo "<td> <a href='Edit_Member.php?update_s_no_id=". $s_no ."' class='btn btn-warning btn-icon-split'> <span class='icon text-white-50'> <i class='fas fa-fw fa-wrench'></i> </span>
                                         <span class='text'>Edit</span> </a></td>";
                                        echo "<td> 
                                        <a onclick='confirm('Are you sure you want to delete?');' href='?delete_s_no=" . $s_no . "' class='btn btn-danger btn-icon-split'> 
                                            <span class='icon text-white-50'> 
                                                <i class='fas fa-trash'></i> 
                                            </span>
                                            <span class='text'>delete</span> 
                                        </a>
                                    </td>";
                                   echo "</tr>"; }
                               }
                           } else {
                               echo "ไม่พบข้อมูล";
                           }
                           
                           $conn->close();
                           ?>

                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>
    <script src="js/demo/chart-bar-demo.js"></script>