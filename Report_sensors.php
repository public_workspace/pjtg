<?php
    $servername = "localhost";
    $username = "root";
    $password = "1234";
    $dbname = "tagpoject";

    try {

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully!";

    } catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TAG Project</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php include('nav.php'); ?>

                    <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Report sensors</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                    <th>No.</th>
                                    <th>Name tag</th>
                                    <th>Device type</th>
                                    <th>Battery</th>
                                    <th>RSSI</th>
                                    <th>distance</th>
                                    <th>Timestamp</th>
                                    <th>Location</th>
                                    </tr>
                                </thead>
                                <tbody>
        
                                <?php
                                    // Connect to the database
                                    $servername = "localhost";
                                    $username = "root";
                                    $password = "1234";
                                    $dbname = "tagpoject";
        
                                    $conn = new mysqli($servername, $username, $password, $dbname);
        
                                    // Check connection
                                    if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                    }
        
                                    // Select data from the sensor table
                                    $sql = "SELECT * FROM sensors";
                                    $result = $conn->query($sql);
        
                                    // Check if data was retrieved successfully
                                    if ($result->num_rows > 0) {
                                        // Loop through each row and display data in the table
                                        while ($row = $result->fetch_assoc()) {
        
                                            $rssi = $row["s_rssi"];
        
                                            // แปลงค่า RSSI เป็นเซนติเมตร
                                            $distance = number_format(pow(10,(-40 - $rssi) / (10 * 2)), 2);
           
                                            
                                                echo "<tr>";
                                                echo "<td>" . $row["s_no"] . "</td>";
                                                echo "<td>" . $row["s_mac_address"] . "</td>";
                                                echo "<td>" . $row["s_device_type"] . "</td>";
                                                echo "<td>" . $row["s_battery"] . "</td>";
                                                echo "<td>" . $row["s_rssi"] . "</td>";
                                                echo "<td>" . $distance . " m.</td>";
                                                echo "<td>" . $row["s_timestamp"] . "</td>";
                                                echo "<td>" . $row["s_location"] . "</td>";                                       
                                                echo "</tr>";
                                            
                                        }
                                    } else {
                                        echo "No data found.";
                                    }
        
                                    // Close the connection
                                    $conn->close();
                                    ?>
        
        
                                </tbody>
                                </table>
                            </div>
                            </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2024</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>
    <script src="js/demo/chart-bar-demo.js"></script>

</body>

</html>