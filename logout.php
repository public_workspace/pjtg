<?php
// เริ่มใช้งาน session
session_start();

// Unset all session 
$_SESSION = array();

// เคลีย session ทั้งหมดที่ set ไว้
session_destroy();

// redirect ไปยังหน้า login
header("Location: login.php");
exit;
?>
