<?php
    require_once('connection.php')
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TAG Project</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php include('nav.php'); ?>


        <?php
            $servername = "localhost";
            $username = "root";
            $password = "1234";
            $dbname = "tagpoject";

            // เชื่อมต่อกับฐานข้อมูลด้วย PDO
            try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e) {
            echo "เกิดข้อผิดพลาดในการเชื่อมต่อ: " . $e->getMessage();
            }

            // จัดการคำขอ update เฉพาะเมื่อมี `updata_id` และเป็นตัวเลข
            if (isset($_REQUEST['updata_id']) && is_numeric($_REQUEST['updata_id'])) {

            try {
                $id = (int)$_REQUEST['updata_id']; // แปลง `id` เป็น integer

                // เตรียม SELECT statement using PDO
                $select_stmt = $conn->prepare("SELECT * FROM member WHERE id = :id");
                $select_stmt->bindParam(':id', $id, PDO::PARAM_INT); // Bind ID parameter

                // Execute the SELECT statement
                $select_stmt->execute();

                // ดึงข้อมูลเป็น associative array
                $row = $select_stmt->fetch(PDO::FETCH_ASSOC);

                // แยกข้อมูลออกเป็นตัวแปร
                if ($row) { // ตรวจสอบว่ามีข้อมูลก่อนแยก
                extract($row);
                } else {
                $errorMag = "Record not found!"; // จัดการกรณีที่ไม่พบ ID
                }

            } catch(PDOException $e) {
                echo $e->getMessage();
            }
            }

            ?>

            <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                    <img class="rounded-circle mt-5" width="150px" src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg">
                    <span class="font-weight-bold">Edogaru</span><span class="text-black-50">edogaru@mail.com.my</span><span> </span>
                </div>
                </div>
                <div class="col-md-5 border-right">
                <div class="p-3 py-5">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Profile Edit</h4>
                    </div>
                    <div class="row mt-1">
                    <div class="col-md-6"><label class="labels">ID</label><input type="text" value="<?php echo $row["id"]; ?>"class="form-control" placeholder="ID" ></div>
                    </div>
                    <div class="row mt-2">
                    <div class="col-md-6"><label class="labels">Name</label><input type="text" name="Member_firstname" class="form-control" value="<?php echo $row["Member_firstname"]; ?>"></div>
                    <div class="col-md-6"><label class="labels">lastname</label><input type="text" name="Member_lastname" class="form-control" value="<?php echo $row["Member_lastname"]; ?>"></div>
                    </div>
                    <div class="row mt-3"> 
                    <div class="col-md-12"><label class="labels">mail</label><input type="text" name="Member_mail" class="form-control" value="<?php echo $row["Member_mail"]; ?>"></div>
                    <div class="col-md-12"><label class="labels">Status</label><input type="text" class="form-control" placeholder="education" value="<?php echo $row["Member_status"]; ?>"></div>
                    </div>
                    <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Updata Profile</button></div>
                </div>
                </div>
            </div>
            </div>



