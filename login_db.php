<?php 
session_start();

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tagpoject";

// สร้างการเชื่อมต่อ
$conn = new mysqli($servername, $username, $password, $dbname);

// ตรวจสอบการเชื่อมต่อ
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// รับค่าที่ส่งมาจากฟอร์ม
$email = $_POST['Member_email'];
$password = $_POST['Member_password'];

// ค้นหาผู้ใช้จากฐานข้อมูล
$sql = "SELECT * FROM member WHERE Member_email = '$email' AND Member_password = '$password'";
$result = $conn->query($sql);

// ตรวจสอบว่ามีผู้ใช้หรือไม่
if ($result->num_rows > 0) {

    $row = $result->fetch_assoc();
    // ตั้งค่าสถานะเข้าสู่ระบบ
    $_SESSION['loggedin'] = true;
    $_SESSION['userid'] = $row['Member_id'];
    $_SESSION['fullname'] = $row['Member_firstname'] . ' ' . $row['Member_lastname'];
    $_SESSION['status'] = $row['Member_status'];
    // เข้าสู่ระบบสำเร็จ
    header("Location: index.php"); // ส่งผู้ใช้ไปยังหน้า home.php
    exit(); // จบการทำงานของ script
} else {
    // ตั้งค่าสถานะเข้าสู่ระบบ
    $_SESSION['loggedin'] = false;
    // เข้าสู่ระบบไม่สำเร็จ
    echo "<script>alert('เข้าสู่ระบบไม่สำเร็จ'); window.location.href = 'login.php';</script>";
}


$conn->close();
/*
    session_start();
    include('connection.php');

    if (isset($_POST['login'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

      
        if (empty($email)) {
            $_SESSION['error'] = 'กรุณากรอกอีเมล';
            header("location: login.php");
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error'] = 'รูปแบบอีเมลไม่ถูกต้อง';
            header("location: login.php");
        } else if (empty($password)) {
            $_SESSION['error'] = 'กรุณากรอกรหัสผ่าน';
            header("location: login.php");
        } else if (strlen($_POST['password']) > 20 || strlen($_POST['password']) < 5) {
            $_SESSION['error'] = 'รหัสผ่านต้องมีความยาวระหว่าง 5 ถึง 20 ตัวอักษร';
            header("location: login.php");
        } else {
            try {

                $check_data = $conn->prepare("SELECT * FROM member WHERE Member_email = :email");
                $check_data->bindParam(":email", $email);
                $check_data->execute();
                $row = $check_data->fetch(PDO::FETCH_ASSOC);

                if ($check_data->rowCount() > 0) {

                    if ($email == $row['email']) {
                        if (password_verify($password, $row['password'])) {
                            if ($row['urole'] == 'admin') {
                                $_SESSION['admin_login'] = $row['id'];
                                header("location: index.php");
                            } else {
                                $_SESSION['user_login'] = $row['id'];
                                header("location: index.php");
                            }
                        } else {
                            $_SESSION['error'] = 'รหัสผ่านผิด';
                            header("location: login.php");
                        }
                    } else {
                        $_SESSION['error'] = 'อีเมลผิด';
                        header("location: login.php");
                    }
                } else {
                    $_SESSION['error'] = "ไม่มีข้อมูลในระบบ";
                    header("location: login.php");
                }

            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
    }

*/
?>
